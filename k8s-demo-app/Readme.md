[DevOps](https://blog.prageesha.com/)

-   [Home](https://blog.prageesha.com/)
-   [Google Cloud
    Platform](https://blog.prageesha.com/tag/google-cloud-platform/)
-   [About Me](https://prageesha.com/)
-   [AWS](https://blog.prageesha.com/tag/amazon-web-services/)

Kubernetes Demo App using GKE

[](https://www.facebook.com/prageesha "Facebook")

[](https://feedly.com/i/subscription/feed/https://blog.prageesha.com/rss/ "RSS")

[Kubernetes](https://blog.prageesha.com/tag/kubernetes/)

Kubernetes Demo App using GKE {.post-full-title}
=============================

-   ![Prageesha
    Galagedara](./Kubernetes%20Demo%20App%20using%20GKE_files/prageesha.jpg)

    Prageesha Galagedara
    --------------------

    Read [more posts](https://blog.prageesha.com/author/prageesha/) by
    this author.

    [![Prageesha
    Galagedara](./Kubernetes%20Demo%20App%20using%20GKE_files/prageesha.jpg)](https://blog.prageesha.com/author/prageesha/)

#### [Prageesha Galagedara](https://blog.prageesha.com/author/prageesha/) {.author-name}

15 Nov 2020 • 4 min read

![Kubernetes Demo App using
GKE](./Kubernetes%20Demo%20App%20using%20GKE_files/carlos-muza-hpjSkU2UYSU-unsplash.jpg)

Today, I am going to show a demo app which shows you the running
containers of your GKE cluster. There was a time that we need to
demonstrate Kubernetes to several parties, and we find it difficult to
show them graphically when we do scale up and scale down.

There is cool Hexboard app which demonstrate during a Redhat Openshift
demo, but it was keep crashing when I do a refresh. However, I built
small dashboard using python which shows the number of pods in a
namespace, and it  keeps changing when you do scale up and scale down.

This setup has following components.

1.  Dashboard App (grid-app)
2.  App which get scale up and down (demo app)
3.  A new k8s namespace (k8sdemo)
4.  Service accounts and Roles

Namespace and Service account {#namespaceandserviceaccount}
-----------------------------

We create a new k8s namespace (k8sdemo) to deploy the "demo app" and
create a service account (demoadmin) and grant "cluster view" permission
to that service account, so that service account can query k8s api and
get the pods details. Use following yaml file and do 'kubectl apply -f
\<file-name.yaml\>' to create those resources.

Do I need to use the same namespace name? Yes, because my python app is
hard coded to read the pods in the 'k8sdemo' namespace, I could
parameterized it but was bit lazy :D

    ---
    apiVersion: v1
    kind: Namespace
    metadata:
      name: k8sdemo
      labels:
        name: k8sdemo

    ---
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: demoadmin  
      namespace: k8sdemo


    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: demoadmin
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: view
    subjects:
      - kind: ServiceAccount
        name: demoadmin
        namespace: k8sdemo

Dashboard app (grid app) to demo pods
-------------------------------------

Use following deployment file to deploy the dashboard app, make sure you
deploy this to any other namespace than k8sdemo, because if you deployed
it to k8sdemo namespace pod result will be included this "grid-app"
also.

How to get the token value:

Run following command to get the token value of the service account, you
need this value pass to the below deployment (for K8S\_TOKEN).

    $ kubectl -n k8sdemo describe secret $(kubectl -n k8sdemo get sa | grep demoadmin | awk '{print $1}')

![](./Kubernetes%20Demo%20App%20using%20GKE_files/token-value.png)

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: grid-app
      namespace: default
      labels:
        app: grid-app
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: grid-app
      template:
        metadata:
          labels:
            app: grid-app
        spec:
          containers:
          - name: grid-app
            image: prageesha/k8s-demo
            env:
            - name: K8S_API
              value: "https://kubernetes.default"
            - name: K8S_TOKEN
              value: "<Token Value>"
            ports:
            - containerPort: 5000
              name: "grid-apptcp"
              protocol: "TCP"
            resources:
              requests:
                memory: "64Mi"
                cpu: "10m"
    ---

    apiVersion: v1
    kind: Service
    metadata:
      name: grid-app
      namespace: default
    spec:
      type: ClusterIP
      selector:
        app: grid-app
      ports:
      - protocol: TCP
        port: 80
        targetPort: 5000

Create grid app using above YAML file.

    kubectl apply -f k8s-demo.yaml

![](./Kubernetes%20Demo%20App%20using%20GKE_files/grid-app-pod.png)

Running App

### Expose your grid-app

In order to access this app you need to expose it as a loadbalancer, or
anyother method that you use in you Kubernetes cluster to access the
apps. In this case I am going to do a Port Forwarding in my service for
testing.

![](./Kubernetes%20Demo%20App%20using%20GKE_files/port-forwarding.png)

Once you click on PORT FORWARDING you can see it will create a cloud
shell and run the port forwarding command. Now you can access the
application using "Open in web preview" button.

![](./Kubernetes%20Demo%20App%20using%20GKE_files/open-in-web.png)

You should see a demo like this. Don't worry there is nothing because we
have not created our demo app yet.

![](./Kubernetes%20Demo%20App%20using%20GKE_files/home-page.png)

Creating our demo pod
---------------------

Use following pod.yaml to showcase scale up and scale down.

pod.yaml

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: demo
      namespace: k8sdemo
      labels:
        app: demo
    spec:
      replicas: 4
      selector:
        matchLabels:
          app: demo
      template:
        metadata:
          labels:
            app: demo
        spec:
          containers:
          - name: demo
            image: k8s.gcr.io/goproxy:0.1
            readinessProbe:
              tcpSocket:
                port: 8080
              initialDelaySeconds: 10
              periodSeconds: 20
            livenessProbe:
              tcpSocket:
                port: 8080
              initialDelaySeconds: 15
              periodSeconds: 20
            ports:
            - containerPort: 8080
            resources:
              requests:
                memory: "16Mi"
                cpu: "5m"
    ---

    apiVersion: v1
    kind: Service
    metadata:
      name: demo
      namespace: k8sdemo
    spec:
      type: ClusterIP
      selector:
        app: demo
      ports:
      - protocol: TCP
        port: 8080
        name: "demotcp"
        targetPort: 8080
        nodePort: 0

    kubectl apply -f pod.yaml

When you create above deployment you must see that your grid-app is
showing those pods like below.

![](./Kubernetes%20Demo%20App%20using%20GKE_files/apply.png)

Pods are getting created

![](./Kubernetes%20Demo%20App%20using%20GKE_files/get-pods.png)

kubectl -n k8sdemo get pods

Scale up your deployment
------------------------

    kubectl -n k8sdemo scale  --replicas=20 deployment/demo

![](./Kubernetes%20Demo%20App%20using%20GKE_files/get-pods-20.png)

![](./Kubernetes%20Demo%20App%20using%20GKE_files/scale.png)

Pods are creating and coming to "running" state

Delete pods to observe the K8S Demo scale down and up
-----------------------------------------------------

    kubectl -n k8sdemo delete pods --all

![](./Kubernetes%20Demo%20App%20using%20GKE_files/delete.png)

pods get deleting

Pods are getting deleted and recreated. Green ones are the running ones
and the Blue ones are the ones in not running state.

Finally it settled to 20 pods because we scaled our deployment to 20
pods earlier.

![](./Kubernetes%20Demo%20App%20using%20GKE_files/back-ro-20.png)

Back to 20 running pods

Clean Up
--------

Delete all the resource you created using following command and yaml
files.

    kubectl delete -f ./pod.yaml 

    kubectl delete -f ./k8s-demo.yaml

    kubectl delete -f ./sa.yaml 

![](./Kubernetes%20Demo%20App%20using%20GKE_files/delete-all.png)

Delete all

### More in [Kubernetes](https://blog.prageesha.com/tag/kubernetes/)

-   #### [Production grade GKE cluster](https://blog.prageesha.com/production-grade-gke-cluster/)

    25 Jul 2020 – 6 min read

-   #### [Running GKE cluster under 10\$/Month: Part 3 Access App](https://blog.prageesha.com/run-on-gke-part-3/)

    15 Nov 2019 – 3 min read

-   #### [Running GKE cluster under 10\$/Month: Part 2](https://blog.prageesha.com/run-on-gke-part-2/)

    9 Oct 2019 – 4 min read

[See all 7 posts →](https://blog.prageesha.com/tag/kubernetes/)

[![Production grade GKE
cluster](./Kubernetes%20Demo%20App%20using%20GKE_files/prod-grade-gke.jpg)](https://blog.prageesha.com/production-grade-gke-cluster/)

[](https://blog.prageesha.com/production-grade-gke-cluster/)

Cloud Computing

Production grade GKE cluster {.post-card-title}
----------------------------

In this post, I am planning to discuss what are the things that we want
to decide when creating a GKE cluster for your production environment.
There are several flavours

-   Prageesha Galagedara
    [![Prageesha
    Galagedara](./Kubernetes%20Demo%20App%20using%20GKE_files/prageesha.jpg)](https://blog.prageesha.com/author/prageesha/)

[Prageesha Galagedara](https://blog.prageesha.com/author/prageesha/) 25
Jul 2020 • 6 min read

[DevOps](https://blog.prageesha.com/) © 2020

[Latest Posts](https://blog.prageesha.com/)
[Facebook](https://www.facebook.com/prageesha)
[Ghost](https://ghost.org/)